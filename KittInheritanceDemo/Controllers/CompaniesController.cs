﻿using System.Collections.Generic;
using System.Web.Mvc;
using KittInheritanceDemo.ViewModels;
using KittInheritanceDemo.ViewModels.Shared;

namespace KittInheritanceDemo.Controllers
{
    public class CompaniesController : Controller
    {

        private IDictionary<int, CompanyViewModel> GetCompanies()
        {
            var companies = new Dictionary<int, CompanyViewModel>();

            var company1 = new CompanyViewModel
            {
                CompanyId = 1,
                Name = "Global Tetrahedron",
                IsActive = true
            };

            var company2 = new CompanyViewModel
            {
                CompanyId = 2,
                Name = "Ben & Jerry's St. Albans",
                ParentCompanyId = 3,
                ParentCompanyName = "Ben & Jerry's",
                IsActive = true
            };

            var company3 = new CompanyViewModel
            {
                CompanyId = 3,
                Name = "Ben & Jerry's",
                IsActive = true
            };

            companies.Add(company1.CompanyId, company1);
            companies.Add(company2.CompanyId, company2);
            companies.Add(company3.CompanyId, company3);

            return companies;
        }

        public ActionResult Index()
        {
            var vm = GetCompanies().Values;
            return View(vm);
        }

        public ActionResult Details(int id)
        {
            var companies = GetCompanies();
            var vm = companies[id];

            vm.Breadcrumbs.Add(new Breadcrumb("Companies", Url.Action("Index", "Companies")));
            if (vm.ParentCompanyId.HasValue)
            {
                vm.Breadcrumbs.Add(new Breadcrumb(vm.ParentCompanyName, Url.Action("Details", "Companies", new { id = vm.ParentCompanyId.Value})));
            }
            vm.Breadcrumbs.Add(new Breadcrumb(vm.Name, null));

            return View(vm);
        }
    }
}
