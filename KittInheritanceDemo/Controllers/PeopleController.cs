﻿using System.Collections.Generic;
using System.Web.Mvc;
using KittInheritanceDemo.ViewModels;
using KittInheritanceDemo.ViewModels.Shared;

namespace KittInheritanceDemo.Controllers
{
    public class PeopleController : Controller
    {
        private IDictionary<int, PersonViewModel> GetPeople()
        {
            var people = new Dictionary<int, PersonViewModel>();

            var person1 = new PersonViewModel
            {
                PersonId = 1,
                FirstName = "Kilgore",
                LastName = "Trout",
                CompanyId = 1,
                CompanyName = "Global Tetrahedron"
            };

            var person2 = new PersonViewModel()
            {
                PersonId = 2,
                FirstName = "Billy",
                LastName = "Pilgrim"
            };

            var person3 = new PersonViewModel()
            {
                PersonId = 3,
                FirstName = "Dwayne",
                LastName = "Hoover",
                CompanyId = 2,
                CompanyName = "Ben & Jerry's St. Albans",
                ParentCompanyId = 3,
                ParentCompanyName = "Ben & Jerry's"
            };

            people.Add(person1.PersonId, person1);
            people.Add(person2.PersonId, person2);
            people.Add(person3.PersonId, person3);

            return people;
        }

        public ActionResult Index()
        {
            var vm = GetPeople().Values;
            return View(vm);
        }

        public ActionResult Details(int id)
        {
            var people = GetPeople();
            var vm = people[id];

            vm.Breadcrumbs.Add(new Breadcrumb("People", Url.Action("Index", "People")));
            if (vm.ParentCompanyId.HasValue)
            {
                vm.Breadcrumbs.Add(new Breadcrumb(vm.ParentCompanyName, Url.Action("Details", "Companies", new { id = vm.ParentCompanyId})));
            }
            if (vm.CompanyId.HasValue)
            {
                vm.Breadcrumbs.Add(new Breadcrumb(vm.CompanyName, Url.Action("Details", "Companies", new { id = vm.CompanyId.Value})));
            }
            vm.Breadcrumbs.Add(new Breadcrumb(vm.FullName, null));

            return View(vm);
        }
    }
}
