﻿namespace KittInheritanceDemo.ViewModels.Shared
{
    public class Breadcrumb
    {
        public Breadcrumb(string linkText, string linkUrl)
        {
            LinkText = linkText;
            LinkUrl = linkUrl;
        }

        public string LinkText { get; private set; }
        public string LinkUrl { get; private set; }
    }
}