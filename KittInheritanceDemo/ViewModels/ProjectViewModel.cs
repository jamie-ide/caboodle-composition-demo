﻿namespace KittInheritanceDemo.ViewModels
{
    public class ProjectViewModel
    {
        public int ProjectId { get; set; }
        public string Title { get; set; }
    }
}