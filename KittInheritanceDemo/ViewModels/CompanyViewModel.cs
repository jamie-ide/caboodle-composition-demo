﻿using System.Collections.Generic;
using KittInheritanceDemo.ViewModels.Shared;

namespace KittInheritanceDemo.ViewModels
{
    public class CompanyViewModel : ICoreEntityHeaderViewModel
    {
        public CompanyViewModel()
        {
            Breadcrumbs = new List<Breadcrumb>();
        }

        public int CompanyId { get; set; }
        public string Name { get; set; }
        public int? ParentCompanyId { get; set; }
        public string ParentCompanyName { get; set; }

        public int Id
        {
            get { return CompanyId; }
        }

        public string HeaderText
        {
            get { return Name; }
        }

        public bool IsActive { get; set; }

        public IList<Breadcrumb> Breadcrumbs { get; set; }
    }
}