﻿using System.Collections.Generic;
using System.Web;
using KittInheritanceDemo.ViewModels.Shared;

namespace KittInheritanceDemo.ViewModels
{
    public interface ICoreEntityHeaderViewModel
    {
        int Id { get; }
        string HeaderText { get; }
        bool IsActive { get; }
        IList<Breadcrumb> Breadcrumbs { get; } 
    }
}