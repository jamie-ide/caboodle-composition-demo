﻿using System.Collections.Generic;
using System.Web;
using KittInheritanceDemo.ViewModels.Shared;

namespace KittInheritanceDemo.ViewModels
{
    public class PersonViewModel : ICoreEntityHeaderViewModel
    {
        public PersonViewModel()
        {
            Breadcrumbs = new List<Breadcrumb>();
        }

        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int? ParentCompanyId { get; set; }
        public string ParentCompanyName { get; set; }

        public int Id
        {
            get { return PersonId; }
        }

        public string HeaderText
        {
            get { return FullName; }
        }

        public bool IsActive { get; set; }

        public IList<Breadcrumb> Breadcrumbs { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}